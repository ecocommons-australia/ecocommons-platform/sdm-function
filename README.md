# SDM function

The SDM function repository contains [Nextflow](https://nextflow.io) pipelines and 
R integration scripts for the EcoCommons SDM, Climate Change and other models.

## Nextflow pipelines and examples
See [README.pipelines.md](./README.pipelines.md)

## Local development
See [README.localdev.md](./README.localdev.md)

## Nextflow DSL2
From tag `1.2.10-1` the pipelines use Nextflow DSL v2. 
This has been supported by Nextflow for some time, however new versions have dropped support for DSL v1.  
Nextflow will need to be downgraded to `22.10.0` when running older pipeline revisions.

```bash
NXF_VER=22.10.0 nextflow run ann.nf ...
```

## SDM Functions (SDM R scripts)
[SDM Function doc](./README.pipelines.md)

## Docker images

<img src="docs/resources/containers.png"  width="800">

### SDM Function Utils (Platform wrapper, Python)
SDM Function Utils Docker images are built via the Gitlab pipeline.  
This image provides platform support and integration for scientific functions.

### SDM Function (Python, R)
SDM Function Docker image Bundles the platform integration with the *EcoCommons R suite (ECR)*, which is the base image.

# Deployment

SDM Function Docker images are built via the Gitlab pipeline.  
The protected branches `develop` and `master` build with predefined image tags and are 
automatically synced with the Dev and Test execution clusters.

### Development
The most recent `ECR package` build (`latest`) can be automatically deployed to the Development environment.
This can be triggered at any time by using "Pipelines > Run Pipeline" and choosing the `develop` branch.  
Note, the ECR repository pipeline must run to completion before this step can be actioned.

### Production 
Production releases must be manually tagged in Gitlab.

SDM Function uses the following Docker tag convention for deployment to specific environments.

### Tagging
Git ref (branch/tag) | Docker Tag | Target environment
---------------------|------------|---------------------
develop              | develop    | Development (dev.*.org.au)
master               | master     | Test (test.*.org.au)
1.0.0-1              | 1.0.0-1    | Production (app.*.org.au)


Tags should be aligned with the ECR package version with an additional version postfix. ie 

- ECR 1.2.3 -> 1.2.3
- ECR 1.2.3 -> 1.2.3-1 (subsequent modification to SDM-FUNC integration)

## Nextflow profiles and versions

The repository `nextflow.config` file is configured with two *profiles* for different execution cluster environments. 

Production uses the **standard** profile, which should be configured using Semver image tags as indicated above.

```
standard { 
    process { 
        .... 
        withName:runSDM { 
            container = 'registry.gitlab.com/ecocommons-australia/ecocommons-platform/sdm-function:1.10.0' 
        } 
    } 
} 
```

Note, `latest`, `master` or similar should NEVER be used as the **standard** tag as they are an unversioned and transient resource.

## Production Deployment
Historically the deployment cutover has been determined by the `nextflow.config` profile committed 
on the **master** branch. 
This is because the execution cluster reads from the default Git branch unless told otherwise.  
This isn't ideal as it undermines versioning for the platform pipeline execution, and may cause 
inadvertent production issues when merging code to **master**.  

The solution is to provide the intended deployment ref from the pipeline caller itself, ie the platform. 
And use tags instead of the master branch. 
This is managed by setting the following within the platform deployment config.

https://gitlab.com/ecocommons-australia/ecocommons-platform/kubernetes-cluster/-/blob/main/overlays/production/frontend/bccvl-job-composer/node.env?ref_type=heads

```
NEXT_PUBLIC_BCCVL_JOB_COMPOSER_DEFAULT_PIPELINE_REV=1.10.0
NEXT_PUBLIC_BCCVL_JOB_COMPOSER_DEFAULT_PIPELINE_PROFILE=standard
```

## Dev/Test Deployment 
Dev and Test environments use the **versioned** profile, which expects the NF executor to provide 
the envvar `JOB_VERSION` with a value matching a specific Git reference and image tag.  
This allows these environments to execute new builds using the **develop** and **master** refs automatically.


### Pipeline image prefetch
The pipelines prefech various Docker images into the cluster environments.  

For Dev/Test, successful builds immediately become available for use. For Production a new tagged 
build will be staged, but it wont become the active deployment until the Nextflow profile 
is updated and the client provides the new pipeline rev.


