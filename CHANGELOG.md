## 
- Rework pipelines to use NEXTFLOW DSL2 which has been required for all new versions of NF for some time.
- Remove hard coded scratch space paths so NF can properly abstract storage during pipeline process execution, in other words, data now flows through the NF pipeline natively instead of using side channels.
- Consolidate pipeline code since its 99% the same.

## 1.2.10-1
- Upgrade django-func-utils to fix ALA geojson issues

## 1.2.10
- ECR 1.2.10 - See NEWS.md

## 1.2.9-1
- Use pipeline implementation for region constraint fetching instead of JobManAPI. If Tempurl is not available in region param, the pipeline will fetch it directly and apply geojson optimisation.   Function params must be individually configured to use this via `transforms: ['fetch_ala_geojson']`

## 1.2.9
- ECR-1.2.9 (base_layer, prediction_region, fixes)
- Updated schemas to add `base_layer` param

## 1.2.8-2
- Import all schemas to the Function Manager API
- Rework schema canonical ids
- Rename schema 'dependencies' to '$dependencies' to avoid using a jsonschema keyword

## 1.2.8-1
- ECR 1.2.8 - See ecocommons/NEWS.md

## 1.2.7-2
- Implement max retries/exp backoff for data fetches for Function execution prepare params stage.

## 1.2.7-1
- ECR R 1.2.7
- Use django-func-utils for prepare/upload operations

## 1.2.6-1 
- ECR R 1.2.6 - Additional constraint fixes

## 1.2.5-1
- Fix issues with SDM and CC constraint and extent calculation

## 1.2.4
- Remove incorrect/inappropriate outputs from Climatch, RB, Bioclim, SRE [BC-330] 
- Update BSSDM 0.1.4.  Fix issues with RangeBagging, Climatch not working correctly in CC experiment

## 1.2.3
- ECR 1.2.5
- Fix for Result data Coverage generation. Will now correctly read and store the geotiff CRS metadata.

## 1.2.2
- Allow Func integrations scripts to be placed in namespace dir. eg `function=bsrmap/func` -> `toolkit/bsrmap/func/func.R`

## 1.2.1
- Fix for metadata assuming `layers` always exists for dataset_layers interface

## 1.2.0
- Allow prepare_inputs `dataset_layers` interface to support `source`: `external` with `url`
- Allow prepare_inputs to search for schemas in `docs/` and `docs/{func_name}/` for better organisation of schemas 

## 1.1.7
- Improve exception handling

## 1.1.1
- Use Pandas.read_csv na_filter=False when parsing CSV to create Result meta. This means blank, 'NA' and other Nanish strings will be processed as is.
- Make Utils deployment aware so overrides can be applied in CI/testing [bc-116]
- Override for parameter 'download_as' handling, ie geojson download to allow for isolated testing without a JobRequest being created [bc-116]
- Improve comments, some var names
