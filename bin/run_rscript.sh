#!/usr/bin/env bash
# LOCAL DEV

# Execute and debug R scripts locally within a nextflow environment and container.
# This expects the appropriate Nextflow pipeline has been run at least once prior 
# (run_pipeline.sh) to setup the script data and parameter environment.

# SCRIPT_NAME refers to the script file in the nextflow workdir.
# {workdir}/script/{script}.R

set -e

[[ -z $1 ]] && printf "Usage: run_rscript.sh SCRIPT_NAME WORKDIR\n\n" && exit 1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
WD=$( cd "${2}" &> /dev/null && pwd )
DOCKER_IMAGE=sdm-function/sdm:latest
NXF_ENVFILE=${NXF_ENVFILE:-"${SCRIPT_DIR}/../.env"}

docker run -v "${WD}:/workdir" --rm --env-file ${NXF_ENVFILE} $DOCKER_IMAGE bash -c "cd /workdir/script/; R < /workdir/script/$1 --no-save"