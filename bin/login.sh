#!/bin/bash
set -e
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Create virtual env
if [ ! -d ${SCRIPT_DIR}/.venv ]; then
	python3 -m venv ${SCRIPT_DIR}/.venv
	${SCRIPT_DIR}/.venv/bin/python -m pip install oauth2_cli_auth==1.4.0 python-dotenv==1.0.1
fi

ENVFILE=${SCRIPT_DIR}/../.env
ENVFILE_AUTH=${SCRIPT_DIR}/../.env.auth

export KEYCLOAK_TOKEN=$($SCRIPT_DIR/.venv/bin/python $SCRIPT_DIR/py/login.py $ENVFILE)

echo "KEYCLOAK_TOKEN=${KEYCLOAK_TOKEN}" > $ENVFILE_AUTH