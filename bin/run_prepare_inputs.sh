#!/usr/bin/env bash
# LOCAL DEV

# Run the prepare_inputs script locally. 
# This mounts the django-func-utils repo directly for buildless debugging (enable in docker-compose.yaml).

set -e

[[ -z $1 || -z $2 ]] && printf "Usage: prepare_inputs.sh FUNC_NAME PARAMS (container relative) [any additional args for prepare_inputs.py]\n\n" && exit 1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
WORKDIR="/work/"
FUNC_NAME=$1
PARAMS=$2
JOB_ID=${3:-"709e17c0-aa71-11ec-916d-0a580a641575"}
#DOCKER_CMD_ARGS=""
#DEBUG=True

docker-compose run --rm $DOCKER_CMD_ARGS -e DEBUG=$DEBUG utils \
	/srv/app/manage.py runscript prepare_inputs --script-args \
	function=$FUNC_NAME \
	params=$PARAMS \
	workdir=$WORKDIR \
	${@:3}