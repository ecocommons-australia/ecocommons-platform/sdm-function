#!/usr/bin/env bash
# LOCAL DEV

# Example: ./bin/run_pipeline.sh bsspread.nf src/docs/examples/bsspread-example.json

set -e

[[ -z $1 || -z $2 ]] && printf "Usage: run_pipeline.sh PIPELINE_PATH PARAMS_PATH [JOB_ID]\n\n" && exit 1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
TIMESTAMP=$(date +%s)
PIPELINE=$1
PARAMS=$2
JOB_ID=${3:-"709e17c0-aa71-11ec-916d-0a580a641575"} # Catch all dev env jobid
AUTH_ENVFILE=${AUTH_ENVFILE:-"${SCRIPT_DIR}/../.env.auth"}
NXF_ENVFILE=${NXF_ENVFILE:-"${SCRIPT_DIR}/../.env"}
NXF_WORKDIR=${SCRIPT_DIR}/../nxf_work/
export NXF_ENVFILE

# Cleanup previous runs
rm -rf ${NXF_WORKDIR}/* ${SCRIPT_DIR}/../.nextflow.*.log

echo "using workdir: ${NXF_WORKDIR}"
echo "using env: ${NXF_ENVFILE} ${AUTH_ENVFILE}"

# Create AUTH_ENVFILE if it does not exist
touch ${AUTH_ENVFILE} || true

# Run pipeline
nextflow run $PIPELINE \
    -w ${NXF_WORKDIR} \
    -profile ${NXF_PROFILE:-"local"} \
    -params-file $PARAMS \
    -name "job_${TIMESTAMP}" \
    -main-script $PIPELINE \
    -with-report "report.html" \
    -latest 1 \
    --absWorkdir '/workdir' \
    --job_uuid $JOB_ID \
    --jobUuid $JOB_ID \
    --inputs $PARAMS \
    --scratch false \
    --upload false \
    --containerOptions "--env-file ${NXF_ENVFILE} --env-file ${AUTH_ENVFILE} --volume ${NXF_WORKDIR}:/workdir" $NXF_ARGS \

# nextflow log "job_${TIMESTAMP}" -f stdout,stderr