#!/usr/bin/env python3
import os
import sys
import argparse
import json
import base64
import urllib
import webbrowser
from urllib.parse import urljoin
from dotenv import load_dotenv
from oauth2_cli_auth import (
    get_auth_url,
    OAuth2ClientInfo,
    OAuthCallbackHttpServer
)
from oauth2_cli_auth.code_grant import exchange_code_for_response

# Local port the temporary httpd server runs on
CALLBACK_SERVER_PORT = 8080

try:
    parser=argparse.ArgumentParser(description='OpenID login')
    parser.add_argument('envfile')
    parser.add_argument('token_type', nargs='?')
    args=parser.parse_args()

    if args.envfile:
        load_dotenv(args.envfile)

    server_url = os.getenv('KEYCLOAK_SERVER_URL') 
    realm = os.getenv('KEYCLOAK_REALM')
    client_id = os.getenv('KEYCLOAK_CLIENT_ID')

    client_info = OAuth2ClientInfo.from_oidc_endpoint(
        urljoin(server_url, f'realms/{realm}/.well-known/openid-configuration'),
        client_id=client_id,
        scopes=['openid']
    )

    server = OAuthCallbackHttpServer(CALLBACK_SERVER_PORT)
    webbrowser.open(get_auth_url(client_info, server.callback_url))
    code = server.wait_for_code()
    if code is None:
        raise ValueError('No code could be obtained from browser callback page')

    resp = exchange_code_for_response(client_info, server.callback_url, code)
    if args.token_type:
        print(resp.get(args.token_type))
    else:
        print(json.dumps(resp))

except ValueError as e:
    print(f'Failed to obtain token ({str(e)})')
