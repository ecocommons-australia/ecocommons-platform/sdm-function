nextflow.enable.dsl=2

params.function = 'maxent'
params.label    = 'xxbigmem'
params.containerOptions = ' -e JAVA_OPTS=-Xmx8g'

include { 
    Job;
} from './nxf_modules/workflow_job'

workflow {
    Job(
        channel.fromPath(params.inputs),
        params.function,
        params.jobUuid
    )
}