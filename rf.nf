#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "rf"

include { 
    Job;
} from './nxf_modules/workflow_job'

workflow {
    Job(
        channel.fromPath(params.inputs),
        params.function,
        params.jobUuid
    )
}