# Workflow Functions - Dev and debugging

## Prereading
This guide assumes basic familarity with Docker, Docker Compose, command line (bash) and Nextflow.

It is written primarily for MacOS and Linux but should be reproducable using Windows WSL.

## Running a pipeline and R scripts locally

Local development has the following minimum install requirements:
- A local copy of this repository
- Nextflow 
- Docker
- Environment file with platform information (`.env`)

### Create a local copy of this repository

```shell
# Download repository
git clone https://gitlab.com/ecocommons-australia/ecocommons-platform/sdm-function.git

# Navigate to downloaded repository
cd sdm-function
```

### Ensure Python is up to date
The default Python installed on a Mac is usually older.  This process requires at least Python `3.10`.

The easiest way to get a newer Python on Mac is to use [brew](https://brew.sh/).

```shell
brew install python3
```

### Install Nextflow

Nextflow requires an updated version of Java.
Java installed on a Mac by default is probably not going to work. 
The best way to install new versions of Java is with [SdkMan](https://sdkman.io/install).

#### (MacOS only) Install Java 17.0.11 (Temurin) with SdkMan 

```shell
# Ensure Sdkman is installed following the instructions: https://sdkman.io/install
# then run this command to install Java
sdk install java 17.0.11-tem
# Open a new shell for this to take effect
```
#### Install Nextflow binary
```shell
# Nextflow downloads to the folder the install script is run from.
curl -s https://get.nextflow.io | bash
# Copy nextflow to a $PATH location to make it a generally available command.
cp ./nextflow /usr/local/bin
```

### Environment and Authorisation
Executing pipelines involves talking to the platform. As such credentials are required.

An env file `.env.example` configured for the Dev environment is available.

To activate it run create a copy and rename as .env. This can be done using the following shell command:

```shell
# This will copy .env.example and save it as .env
cp .env.example .env
```

It is important to note that this file does not contain any actual credentials. 
You will be prompted for these details after running:

#### Login to the platform

```shell
./bin/login.sh
```  

This will open a browser window with the standard login.

When successfully logged in, a file `.env.auth` will be created containing an access token. 
This will automatically be used when running a pipeline via the below commands (until it expires).


### Build docker containers

Images must first be built with the command.

```shell
docker compose build
```

### Run pipeline 

A pipeline can then be run locally to reproduce execution similar to how it occurs in the platform.

`run_pipeline.sh`  will run any pipeline in the repo eg. `helloworld.nf` with any param.json file.  This will perform all the steps the platform would in terms of pulling the data based on UUIDs, setting up the folder structure all locally.  It will execute all steps except for the final `upload_result` step which may fail.

#### Nextflow versions
By default Nextflow uses `DSL v2`, which is a newer pipeline syntax. The current SDM pipelines are written in `v1` which is no longer supported by default.
This can be solved very simply by specifying an older Nexflow version before running the pipeline, as in the example below.

```shell
export NXF_VER=22.10.0
./bin/run_pipeline.sh ./helloworld.nf ./src/docs/examples/helloworld.params.json
```

Or alternatively:

```shell
./bin/run_job.sh ./helloworld.nf JOB_UUID
```

Artefacts created by the Nextflow job:
```
./nxf_work/input/
./nxf_work/output/
./nxf_work/scripts/
```

### Debuging R scripts locally with platform data.

`run_rscript.sh` is a short cut to ‘re run’ just the R script.  This only works if `run_pipeline.sh` has been executed at least once before hand. It will run the R script in the nextflow work dir `nxf_work/script/script.R`.

The integration script can be tweaked and rerun.  Just be sure to copy any changes made in the workdir script back to the original source.

```shell
./bin/run_rscript.sh SCRIPT_NAME_WITHOUT_PATH
```

This allows for basic debugging but deeper introspection and dev should be done using the integrated RStudio environment.

### Debuging prepare_inputs only

`run_prepare_inputs.sh` executes the prepare_inputs.py script directly and can be used for debugging this process. 

```shell
./bin/run_prepare_inputs.sh FUNC_NAME PARAMS (container relative) [any additional args for prepare_inputs.py]
```

## RStudio integration 

`Dockerfile-rstudio` can be optionally built for local R development directly integrated with a RStudio web IDE.

Requires the following dependency to be unzipped and placed in the repo root under `rocker_scripts/`

https://griffitheduau.sharepoint.com/sites/EcoCommonsAustralia-ImplementationTeam/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FEcoCommonsAustralia%2DImplementationTeam%2FShared%20Documents%2FImplementation%20Team%2FEcoCommons%202020%2D2023%2FACTIVITY%20STREAMS%2FStream%203%20%2D%20Scientific%20and%20Data%20Workflows%2FPlatform%20Testing&viewid=50feca04%2Dca32%2D4c28%2Db55b%2D3a47aed2f3e8


### Setup example

#### Build the RStudio Dockerfile

```bash
docker build -f Dockerfile-rstudio -t rstudio .
```

#### Launch the container (example paths, exact local setup will vary)
This mounts a pipeline work dir previously created with `run_pipeline.sh` for interactive debugging.

```bash
# Start
RHOME=~/dev/
REPO=~/dev/sdm-function
docker run --rm -d --name rstudio -v $RHOME:/home/rstudio/ -v $REPO/nxf_work/:/sdmwork -p 127.0.0.1:8787:8787 -e DISABLE_AUTH=true rstudio

# Stop
docker stop rstudio
```

#### Open the RStudio web IDE 

http://127.0.0.1:8787/


### Debug a pipeline

This assumes `./bin/run_job.sh` or `./bin/run_pipeline.sh` has been run at least once and `./sdmwork` contains the working files.

- Open the RStudio web IDE.
- In *console* run `setwd('/sdmwork/script')`.
- Under *Files* navigate to `/sdmwork/script/` and open the integration R script.
- Select part or all of the script and run with Ctrl+Enter.


## Docker Compose

Docker Compose is configured to help build images and a few supporting tasks. It is not part of Nextflow.
 
Check docker-compose.yml and docker-compose.override.example.yml for build and run options.
