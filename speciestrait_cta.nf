nextflow.enable.dsl=2

params.function = "speciestrait_cta"

include { 
    Job;
} from './nxf_modules/workflow_job'

workflow {
    Job(
        channel.fromPath(params.inputs),
        params.function,
        params.jobUuid
    )
}