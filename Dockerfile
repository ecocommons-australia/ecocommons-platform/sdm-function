# Docker SDM image from EC-R package
ARG BASEIMG_VERSION=1.2.10
FROM registry.gitlab.com/ecocommons-australia/ecocommons-platform/ecocommons/ec-rpkg:${BASEIMG_VERSION}

# Allow base ver to be reported from within container
ARG BASEIMG_VERSION
ENV BASEIMG_VERSION=${BASEIMG_VERSION}
ARG BUILD_ID
ENV BUILD_ID=$BUILD_ID

USER root

# IMPORTANT!  
# The base image R expects a specific version of procps (2:3.3.17)
# and will fail with upgrades.  This should really be moved to the ECR Docker build.
# It should also be documented what the R environment actually needs from proc, as this is not clear.
# 2024.01-RC
RUN apt-get install -y procps

RUN mkdir -p /srv/app
WORKDIR /srv/app

# Install BSSDM package (Required for Range bagging and Climatch algorithms)
RUN R -vanilla -e 'devtools::install_github("cebra-analytics/bssdm@0.1.9", upgrade="never", dependencies = TRUE)'

COPY requirements.txt /srv/app
RUN pip install --upgrade pip && pip install --no-cache-dir -r requirements.txt
COPY ./src /srv/app

# Clean up development libraries
RUN apt-get -y purge g++

CMD python manage.py runscript run_sdm --script-args params=/sdmwork/params.json
