# Local dev/debugging with RStudio server integration.
# http://127.0.0.1:8787

ARG BASEIMG_VERSION=latest
FROM registry.gitlab.com/ecocommons-australia/ecocommons-platform/ecocommons/ec-rpkg:${BASEIMG_VERSION}

USER root

RUN apt-get install -y procps
RUN apt-get -y update

RUN mkdir -p /srv/app
WORKDIR /srv/app

# Install various apps
RUN apt-get install -y --fix-missing ca-certificates lsb-release file git libapparmor1 libclang-dev libcurl4-openssl-dev libedit2 libobjc4 libssl-dev libpq5 psmisc procps python-setuptools pwgen sudo wget yum4

# Install RStudio server
RUN sudo apt-get install -y gdebi-core
RUN wget https://download2.rstudio.org/server/focal/amd64/rstudio-server-2023.12.1-402-amd64.deb
RUN sudo gdebi --n rstudio-server-2023.12.1-402-amd64.deb
RUN rm rstudio-server-2023.12.1-402-amd64.deb


# Configure RStudio server
RUN mkdir -p /rocker_scripts
COPY ./rocker_scripts/* /rocker_scripts
RUN /rocker_scripts/configure_rstudio.sh

# Clean up development libraries
RUN apt-get -y purge g++

EXPOSE 8787

CMD ["/init"]