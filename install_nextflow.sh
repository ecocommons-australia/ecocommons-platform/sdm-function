#!/bin/bash
# Install Nextflow locally for MacOS
set +e 

# Install sdkman
curl -s "https://get.sdkman.io" | bash
source "${SDKMAN_DIR}/bin/sdkman-init.sh"

sdk install java 17.0.6-tem

curl -s https://get.nextflow.io | bash

chmod +x nextflow
sudo mv nextflow /usr/local/bin