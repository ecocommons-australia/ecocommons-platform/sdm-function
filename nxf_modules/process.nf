#!/usr/bin/env nextflow

params.label                = ''
params.containerOptions     = null
params.absWorkdir           = ''

process prepareInputs {
    tag "$job_uuid"

    // docker only
    containerOptions params.containerOptions

    input:
    path params_json
    val function
    val job_uuid

    output:
    path 'input_params.json',   emit: params_json
    path 'input/*',             emit: inputdir, type: 'any'
    path 'script/*',            emit: scriptdir
    val job_uuid

    """
    mkdir -p input script

    python /srv/app/manage.py runscript prepare_inputs --script-args \
        function=$function params=$params_json jobuuid=$job_uuid workdir=$params.absWorkdir

    # Legacy behaviour, absolute path workdir.
    if [ -n "$params.absWorkdir" ]; then
        cp -vrf $params.absWorkdir/input/* input/ || true
        cp -vrf $params.absWorkdir/script script || true
    fi
    """
}

process runSDM {
    tag     "$job_uuid"
    label   params.label

    // docker only
    containerOptions params.containerOptions

    input:
    path params_json
    path inputdir,      stageAs: 'input/*'
    path scriptdir,     stageAs: 'script/*'
    val job_uuid
    
    output:
    path 'output/*',        emit: outputdir
    path 'results.zip',     emit: results_zip
    path 'jobinfo.json',    emit: jobinfo
    val job_uuid

    """
    export TASK_CPUS=${task.cpus}
    python /srv/app/manage.py runscript run_sdm --script-args \
        params=$params_json scriptdir=script cpus=${task.cpus}

    # Legacy behaviour, absolute path workdir.
    if [ -n "$params.absWorkdir" ]; then
        cp -vrf $params.absWorkdir/output output || true
    fi
    """
}

process uploadResult {
    tag "$job_uuid"

    // docker only
    containerOptions params.containerOptions

    input:
    path 'output/*'
    path result_zip
    path job_info
    val job_uuid

    output:
    path 'eval',        optional: true
    path '*.zip',       optional: true
    path '*.log',       optional: true
    path '*.R',         optional: true
    path '*.Rout',      optional: true

    """
    python /srv/app/manage.py runscript upload --script-args \
        uuid=$job_uuid result=$result_zip jobinfo=$job_info
    """
}
