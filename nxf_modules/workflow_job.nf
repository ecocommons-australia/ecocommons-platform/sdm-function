#!/usr/bin/env nextflow
// Execute one platform Job.
// `params.function` must be set before run.

params.function = null
params.upload = true

include { 
    prepareInputs;
    runSDM;
    uploadResult;
} from './process'

workflow Job {
    take: params_json
    take: function
    take: job_uuid
    main:
        prepareInputs(
            params_json, 
            function, 
            job_uuid
        ) | runSDM
        
        if (params.upload == true){
            uploadResult(runSDM.out)
        }
}