nextflow.enable.dsl=2

params.function = "circles"

include { 
    Job;
} from './nxf_modules/workflow_job'

workflow {
    Job(
        channel.fromPath(params.inputs),
        params.function,
        params.jobUuid
    )
}