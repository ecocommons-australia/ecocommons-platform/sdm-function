{
    "name": "Range Bagging",
    "description": "The model building component of an implementation of the range bagging species distribution modelling (SDM) method (Drake, 2015)",
    "algorithm_category": "profile",
    "package_dependencies": "Dismo",
    "input": {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "$id": "https://schema.ecocommons.org.au/sdm/range_bagging.json",
        "title": "Range Bagging SDM Function Parameters",
        "description": "Schema for Range Bagging SDM Function parameters",
        "type": "object",
        "properties": {
            "species": {
                "title": "species occurrence dataset",
                "description": "UUID of species occurrence dataset",
                "type": "string"
            },
            "absence": {
                "title": "species absence dataset",
                "description": "UUID of species absence dataset",
                "type": [
                    "string",
                    "null"
                ],
                "default": null
            },
            "base_layer": {
                "title": "base layer dataset",
                "description": "UUID of bias dataset",
                "type": "object",
                "properties": {
                    "uuid": {
                        "title": "dataset id",
                        "description": "UUID of environmental dataset",
                        "type": "string",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    },
                    "layers": {
                        "title": "layers",
                        "description": "List of layer names",
                        "type": "array",
                        "items": {
                            "description": "Layer name",
                            "type": "string"
                        },
                        "minItems": 1,
                        "uniqueItems": true
                    }
                },
                "required": [
                    "uuid",
                    "layers"
                ],
                "default": null
            },
            "predictors": {
                "title": "predictors",
                "description": "List of environmental dataset UUID with a list of selected layer names.",
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "uuid": {
                            "title": "dataset id",
                            "description": "UUID of environmental dataset",
                            "type": "string",
                            "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                        },
                        "layers": {
                            "title": "layers",
                            "description": "List of layer names",
                            "type": "array",
                            "items": {
                                "description": "Layer name",
                                "type": "string"
                            },
                            "minItems": 1,
                            "uniqueItems": true
                        }
                    },
                    "required": [
                        "uuid",
                        "layers"
                    ]
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "modelling_region": {
                "title": "modelling region",
                "description": "Geojson object describing the geographic constraint region",
                "type": "object",
                "properties": {
                    "geojson": {
                        "type": "string",
                        "description": "Geojson string describing the geographic constraint region"
                    },
                    "method": {
                        "type": "string",
                        "description": "Method selected for generation",
                        "enum": [
                            "convex hull",
                            "pre-defined region",
                            "environmental envelope",
                            "user-drawn",
                            "shapefile"
                        ],
                        "default": "convex hull"
                    },
                    "radius": {
                        "type": "number",
                        "description": "Buffer radius in Km",
                        "minimum": 0.0,
                        "default": 0.0
                    },
                    "predefined_region": {
                        "type": "object",
                        "properties": {
                            "region_type": {
                                "type": "string",
                                "description": "Region type selected for pre-defined region"
                            },
                            "regions": {
                                "type": "array",
                                "items": {
                                    "type": "string"
                                },
                                "description": "Regions selected for pre-defined region",
                                "minItems": 1,
                                "uniqueItems": true
                            }
                        },
                        "required": [
                            "region_type",
                            "regions"
                        ]
                    }
                },
                "required": [
                    "geojson",
                    "method"
                ]
            },
            "scale_down": {
                "title": "scale down",
                "description": "Resample by scaling down the resolution : user-defined; finest-resolution (lowest resolution); coarsest-resolution (highest resolution)",
                "type": "string",
                "default": "finest-resolution"
            },
            "pa_ratio": {
                "title": "absence-presence ratio",
                "description": "ratio of absence to presence points",
                "type": "number",
                "minimum": 0.0,
                "default": 1.0
            },
            "pa_strategy": {
                "title": "pseudo-absence strategy",
                "description": "strategy to generate pseudo-absence points: random; SRE (in sites with contrasting conditions to presences); disk (within a minimum and maximum distance from presences)",
                "type": "string",
                "default": "random",
                "enum": [
                    "random",
                    "sre",
                    "disk"
                ]
            },
            "pa_sre_quant": {
                "title": "pseudo-absence SRE quantile",
                "description": "quantile used for 'SRE' pseudo-absence generation strategy; default is 0.025",
                "type": "number",
                "minimum": 0.0,
                "default": 0.025
            },
            "pa_disk_min": {
                "title": "pseudo-absence disk minimum distance (m)",
                "description": "minimum distance (in metres) to presences for 'disk' pseudo-absence generation strategy",
                "type": "number",
                "minimum": 0.0,
                "default": 0.0
            },
            "pa_disk_max": {
                "title": "pseudo-absence disk maximum distance (m)",
                "description": "maximum distance (in metres) to presences for 'disk' pseudo-absence generation strategy",
                "type": [
                    "number",
                    "null"
                ],
                "minimum": 0.0,
                "default": null
            },
            "random_seed": {
                "title": "random seed",
                "description": "seed used for generating random values. (algorithm parameter)",
                "type": [
                    "integer",
                    "null"
                ],
                "default": null,
                "minimum": -2147483648,
                "maximum": 2147483647
            },
            "n_models": {
                "title": "number of convex",
                "description": "Number of convex hull models to build in sampled environment",
                "type": "integer",
                "default": 100
                },
            "n_dim": {
                "title": "number of dimensions",
                "description": "Number of dimensions (variables) of sampled convex hull models",
                "type": "integer",
                "default": 2
                },
            "sample_prop": {
                "title": "Proportion of environment data rows sampled for fitting",
                "description": "Proportion of environment data rows sampled for fitting",
                "type": "number",
                "default": 0.5
                },
            "limit_occur": {
                "title": "Limit occurances",
                "description": "Logical to indicate whether to limit occurrence data to one per environment data cell",
                "type": "boolean",
                "default": true
                },    
            "species_filter": {
                "title": "species filter",
                "description": "Species names to be included, only for Multi-Species SDM",
                "type": "array",
                "items": {
                    "description": "scientific name of species",
                    "type": "string"
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "subsets": {
                "title": "subsets",
                "description": "list of subset items, for Migratory Modelling",
                "type": "array",
                "items": {"$ref": "#/$defs/subset"},
                "minItems": 1,
                "uniqueItems": true
            },
            "unconstraint_map": {
                "title": "unconstraint map",
                "description": "Indicates whether to generate an unconstraint map or not. True by default.",
                "type": "boolean",
                "default": true
            },
            "generate_convexhull": {
                "title": "generate convex-hull polygon",
                "description": "Indicates to generate and apply a convex-hull polygon of the occurrence dataset to constraint. False by default.",
                "type": "boolean",
                "default": false
            }
        },
        "required": [
            "species",
            "absence",
            "predictors",
            "modelling_region",
            "scale_down",
            "pa_ratio",
            "pa_strategy",
            "pa_sre_quant",
            "pa_disk_min",
            "pa_disk_max",
            "random_seed",
            "limit_occur",
            "n_dim",
            "n_models",
            "sample_prop"    
        ],
        "$defs": {
            "subset": {
                "title": "subset",
                "description": "subset for Migratory Modelling",
                "type": "array",
                "items": [
                    {"$ref": "#/$defs/subset_name"},
                    {"$ref": "#/$defs/month_filter"},
                    {"$ref": "#/$defs/subset_predictors"}
                ]
            },
            "subset_name": {
                "title": "subset name",
                "description": "name for a subset, only for Migratory Modelling",
                "type": "string"
            },
            "month_filter": {
                "title": "month filter",
                "description": "List of numerical months to use, only for Migratory Modelling",
                "type": "array",
                "items": {
                    "description": "Numerical month (1-12)",
                    "type": "integer",
                    "minimum": 1,
                    "maximum": 12
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "subset_predictors": {
                "type": "array",
                "title": "subset predictors",
                "description": "List of predictor items",
                "items": {
                    "$ref": "#/$defs/subset_predictor"
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "subset_predictor": {
                "title": "subset predictor",
                "description": "a reference to a selected curated dataset with some layers",
                "type": "object",
                "properties": {
                    "uuid": {
                        "title": "dataset id",
                        "type":  "string",
                        "description": "uuid of dataset in the database",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    },
                    "layers": {
                        "title": "layers",
                        "type": "array",
                        "items": {
                            "type": "string",
                            "description": "name of layer in the dataset"
                        },
                        "minItems": 1,
                        "uniqueItems": true
                    }
                },
                "required": ["uuid", "layers"]
            }
        }
    },
    "output": {
        "*proj_current*.tif": {
            "title": "Projection to current climate",
            "mimetype": "image/geotiff",
            "layer": "projection_probability",
            "data_type": "Continuous"
        },
        "*proj_current*_unconstrained.tif": {
            "title": "Projection to current climate - unconstrained",
            "mimetype": "image/geotiff",
            "layer": "projection_probability",
            "data_type": "Continuous"
        },
        "proj_current_*.png": {
            "title": "Projection plot",
            "mimetype": "image/png"
        },
        "proj_current_*_unconstrained.png": {
            "title": "Projection plot - unconstrained",
            "mimetype": "image/png"
        },
        "occurrence_environmental_*.csv": {
            "title": "Occurrence points with environmental data",
            "mimetype": "text/csv"
        },
        "absence_environmental_*.csv": {
            "title": "Absence points with environmental data",
            "mimetype": "text/csv"
        },
        "response_curve_*.png": {
            "title": "Marginal Response Curve",
            "mimetype": "image/png"
        },
        "*_response_curve_*.png": {
            "title": "Marginal Response Curve",
            "mimetype": "image/png"
        },
        "biomod2_like_VariableImportance_*.csv": {
            "title": "Variable importance table",
            "mimetype": "text/csv"
        },
        "maxent_like_VariableImportance_*.csv": {
            "title": "Variable importance table",
            "mimetype": "text/csv"
        },
        "Evaluation-statistics_*.csv": {
            "title": "Model accuracy statistics",
            "mimetype": "text/csv"
        },
        "dismo-presence-absence-plot_*.png": {
            "title": "Presence/absence density plot",
            "mimetype": "image/png"
        },
        "dismo-presence-absence-hist_*.png": {
            "title": "Presence/absence histogram",
            "mimetype": "image/png"
        },
        "dismo-TPR-TNR_*.png": {
            "title": "Sensitivity/Specificity plot",
            "mimetype": "image/png"
        },
        "dismo-error-rates_*.png": {
            "title": "Error rates plot",
            "mimetype": "image/png"
        },
        "dismo-ROC_*.png": {
            "title": "ROC plot",
            "mimetype": "image/png"
        },
        "dismo-loss-functions_*.png": {
            "title": "Loss functions plot",
            "mimetype": "image/png"
        },
        "Loss-function-intervals-table_*.csv": {
            "title": "Loss functions table",
            "mimetype": "text/csv"
        },
        "dismo-loss-intervals_*.png": {
            "title": "Loss functions intervals",
            "mimetype": "image/png"
        },
        "Evaluation-data_*.csv": {
            "title": "Model evaluation data",
            "mimetype": "text/csv"
        },
        "*.R": {
            "title": "Job Script",
            "mimetype": "text/x-r"
        },
        "dismo.eval.object.RData": {
            "title": "R ModelEvaluation object",
            "mimetype": "application/x-r-data"
        },
        "model.object_*.RData": {
            "title": "R SDM Model object",
            "mimetype": "application/x-r-data"
        },
        "*.Rout": {
            "title": "Log file",
            "mimetype": "text/x-r-transcript"
        },
        "modelling_region.json": {
            "title": "modelling region",
            "mimetype": "text/x-r-transcript"
        },
        "metadata.json": {
            "title": "Metadata",
            "mimetype": "application/json"
        },
        "*.csv": {
            "title": "Model accuracy statistics",
            "mimetype": "text/csv"
        },
        "*.png": {
            "title": "New Model plots",
            "mimetype": "image/png"
        },
        "params.json": {
            "title": "Input parameters",
            "mimetype": "text/x-r-transcript"
        }
    }
}