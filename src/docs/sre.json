{
    "name": "Surface Range Envelope (SRE)",
    "description": "Similar to bioclim: predicts species occurrence probabilities based on a comparison of the values of the environmental variables at new sites, compared to the lower and upper percentiles of the values at locations where the species is known to occur.",
    "algorithm_category": "profile",
    "package_dependencies": "BIOMOD2",
    "input": {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "$id": "https://schema.ecocommons.org.au/sdm/sre.json",
        "title": "Surface Range Envelope (SRE) SDM Function Parameters",
        "description": "Schema for Surface Range Envelope (SRE) SDM Function parameters",
        "type": "object",
        "properties": {
            "species": {
                "title": "species occurrence dataset",
                "description": "UUID of species occurrence dataset",
                "type": "string"
            },
            "absence": {
                "title": "species absence dataset",
                "description": "UUID of species absence dataset",
                "type": [
                    "string",
                    "null"
                ],
                "default": null
            },
            "predictors": {
                "title": "predictors",
                "description": "List of environmental dataset UUID with a list of selected layer names.",
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "uuid": {
                            "title": "dataset id",
                            "description": "UUID of environmental dataset",
                            "type": "string",
                            "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                        },
                        "layers": {
                            "title": "layers",
                            "description": "List of layer names",
                            "type": "array",
                            "items": {
                                "description": "Layer name",
                                "type": "string"
                            },
                            "minItems": 1,
                            "uniqueItems": true
                        }
                    },
                    "required": [
                        "uuid",
                        "layers"
                    ]
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "modelling_region": {
                "title": "modelling region",
                "description": "Geojson object describing the geographic constraint region",
                "type": "object",
                "properties": {
                    "geojson": {
                        "type": "string",
                        "description": "Geojson string describing the geographic constraint region"
                    },
                    "method": {
                        "type": "string",
                        "description": "Method selected for generation",
                        "enum": [
                            "convex hull",
                            "pre-defined region",
                            "environmental envelope",
                            "user-drawn",
                            "shapefile"
                        ],
                        "default": "convex hull"
                    },
                    "radius": {
                        "type": "number",
                        "description": "Buffer radius in Km",
                        "minimum": 0.0,
                        "default": 0.0
                    },
                    "predefined_region": {
                        "type": "object",
                        "properties": {
                            "region_type": {
                                "type": "string",
                                "description": "Region type selected for pre-defined region"
                            },
                            "regions": {
                                "type": "array",
                                "items": {
                                    "type": "string"
                                },
                                "description": "Regions selected for pre-defined region",
                                "minItems": 1,
                                "uniqueItems": true
                            }
                        },
                        "required": [
                            "region_type",
                            "regions"
                        ]
                    }
                },
                "required": [
                    "geojson",
                    "method"
                ]
            },
            "scale_down": {
                "title": "scale down",
                "description": "Resample by scaling down the resolution : user-defined; finest-resolution (lowest resolution); coarsest-resolution (highest resolution)",
                "type": "string",
                "default": "finest-resolution"
            },
            "compress": {
                "title": "compression format",
                "description": "Compression format of objects stored.",
                "type": [
                    "string",
                    "null"
                ],
                "default": "gzip",
                "enum": [
                    "xz",
                    "gzip",
                    null
                ]
            },
            "pa_ratio": {
                "title": "absence-presence ratio",
                "description": "ratio of absence to presence points",
                "type": "number",
                "minimum": 0.0,
                "default": 1.0
            },
            "pa_strategy": {
                "title": "pseudo-absence strategy",
                "description": "strategy to generate pseudo-absence points: random; SRE (in sites with contrasting conditions to presences); disk (within a minimum and maximum distance from presences)",
                "type": "string",
                "default": "random",
                "enum": [
                    "random",
                    "sre",
                    "disk"
                ]
            },
            "pa_sre_quant": {
                "title": "pseudo-absence SRE quantile",
                "description": "quantile used for 'SRE' pseudo-absence generation strategy; default is 0.025",
                "type": "number",
                "minimum": 0.0,
                "default": 0.025
            },
            "pa_disk_min": {
                "title": "pseudo-absence disk minimum distance (m)",
                "description": "minimum distance (m) to presences for 'disk' pseudo-absence generation strategy",
                "type": "number",
                "minimum": 0.0,
                "default": 0.0
            },
            "pa_disk_max": {
                "title": "pseudo-absence disk maximum distance (m)",
                "description": "maximum distance (m) to presences for 'disk' pseudo-absence generation strategy",
                "type": [
                    "number",
                    "null"
                ],
                "minimum": 0.0,
                "default": null
            },
            "random_seed": {
                "title": "random seed",
                "description": "seed used for generating random numbers. (algorithm parameter)",
                "type": [
                    "integer",
                    "null"
                ],
                "default": null,
                "minimum": -2147483648,
                "maximum": 2147483647
            },
            "nb_run_eval": {
                "title": "n-fold cross validation",
                "description": "n-fold cross validation. (algorithm parameter)",
                "type": "integer",
                "minimum": 1,
                "default": 10
            },
            "data_split": {
                "title": "data split",
                "description": "data split. (algorithm parameter)",
                "type": "integer",
                "minimum": 0,
                "maximum": 100,
                "default": 100
            },
            "prevalence": {
                "title": "weighted response weights",
                "description": "allows to give more or less weight to particular observations; default = NULL: each observation (presence or absence) has the same weight; if value < 0.5: absences are given more weight; if value > 0.5: presences are given more weight. (algorithm parameter)",
                "type": [
                    "number",
                    "null"
                ],
                "minimum": 0.0,
                "maximum": 1.0,
                "default": null
            },
            "var_import": {
                "title": "resampling",
                "description": "number of permutations to estimate the importance of each variable. (algorithm parameter)",
                "type": "integer",
                "minimum": 0,
                "default": 0
            },
            "rescale_all_models": {
                "title": "rescale all models",
                "description": "if true, all model prediction will be scaled with a binomial GLM. (algorithm parameter)",
                "type": "boolean",
                "default": false
            },
            "do_full_models": {
                "title": "do full models",
                "description": "if true, will calibrate and evaluate models with the whole dataset. (algorithm parameter)",
                "type": "boolean",
                "default": true
            },
            "quant": {
                "title": "quantile",
                "description": "the quantile used to remove the most extreme values of each environmental variable for determining tolerance boundaries; default is 0.025 corresponding to a 95% confidence interval in normal distributions. (algorithm parameter)",
                "type": "number",
                "minimum": 0.0,
                "maximum": 1.0,
                "default": 0.025
            },
            "species_filter": {
                "title": "species filter",
                "description": "Species names to be included, only for Multi-Species SDM",
                "type": "array",
                "items": {
                    "description": "scientific name of species",
                    "type": "string"
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "subsets": {
                "title": "subsets",
                "description": "list of subset items, for Migratory Modelling",
                "type": "array",
                "items": {"$ref": "#/$defs/subset"},
                "minItems": 1,
                "uniqueItems": true
            },
            "unconstraint_map": {
                "title": "unconstraint map",
                "description": "Indicates whether to generate an unconstraint map or not. True by default.",
                "type": "boolean",
                "default": true
            },
            "generate_convexhull": {
                "title": "generate convex-hull polygon",
                "description": "Indicates to generate and apply a convex-hull polygon of the occurrence dataset to constraint. False by default.",
                "type": "boolean",
                "default": false
            }
        },
        "required": [
            "species",
            "absence",
            "predictors",
            "modelling_region",
            "scale_down",
            "compress",
            "pa_ratio",
            "pa_strategy",
            "pa_sre_quant",
            "pa_disk_min",
            "pa_disk_max",
            "random_seed",
            "nb_run_eval",
            "data_split",
            "prevalence",
            "var_import",
            "rescale_all_models",
            "do_full_models",
            "quant"
        ],
        "$defs": {
            "subset": {
                "title": "subset",
                "description": "subset for Migratory Modelling",
                "type": "array",
                "items": [
                    {"$ref": "#/$defs/subset_name"},
                    {"$ref": "#/$defs/month_filter"},
                    {"$ref": "#/$defs/subset_predictors"}
                ]
            },
            "subset_name": {
                "title": "subset name",
                "description": "name for a subset, only for Migratory Modelling",
                "type": "string"
            },
            "month_filter": {
                "title": "month filter",
                "description": "List of numerical months to use, only for Migratory Modelling",
                "type": "array",
                "items": {
                    "description": "Numerical month (1-12)",
                    "type": "integer",
                    "minimum": 1,
                    "maximum": 12
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "subset_predictors": {
                "title": "subset_predictors",
                "type": "array",
                "description": "List of predictor items",
                "items": {
                    "$ref": "#/$defs/subset_predictor"
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "subset_predictor": {
                "title": "subset predictor",
                "description": "a reference to a selected curated dataset with some layers",
                "type": "object",
                "properties": {
                    "uuid": {
                        "title": "dataset id",
                        "type":  "string",
                        "description": "uuid of dataset in the database",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    },
                    "layers": {
                        "title": "layers",
                        "type": "array",
                        "items": {
                            "type": "string",
                            "description": "name of layer in the dataset"
                        },
                        "minItems": 1,
                        "uniqueItems": true
                    }
                },
                "required": ["uuid", "layers"]
            }
        }
    },
    "output": {
        "Rplots.pdf": {
            "skip": true
        },
        "eval/pROC.Full.png": {
            "skip": true
        },
        "eval/Proj_current_*.tif": {
            "title": "Projection to current climate",
            "genre": "DataGenreCP",
            "mimetype": "image/geotiff",
            "layer": "projection_probability",
            "data_type": "Continuous",
            "order": 1
        },
        "eval/Proj_current_*_unconstrained.tif": {
            "title": "Projection to current climate - unconstrained",
            "genre": "DataGenreCP_ENVLOP",
            "mimetype": "image/geotiff",
            "layer": "projection_probability",
            "data_type": "Continuous",
            "order": 2
        },
        "eval/Proj_current_*.png": {
            "title": "Projection plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 3
        },
        "eval/Proj_current_*_unconstrained.png": {
            "title": "Projection plot - unconstrained",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 4
        },
        "eval/Proj_current_ClampingMask.tif": {
            "title": "Clamping Mask",
            "genre": "DataGenreClampingMask",
            "mimetype": "image/geotiff",
            "layer": "clamping_mask",
            "data_type": "Discrete",
            "order": 5
        },
        "eval_tables/Occurrence_environmental_*.csv": {
            "title": "Occurrence points with environmental data",
            "genre": "DataGenreSpeciesOccurEnv",
            "mimetype": "text/csv",
            "order": 6
        },
        "eval/Mean_response_curves*.png": {
            "title": "Response curves",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 9
        },
        "eval/*_mean_response_curves*.png": {
            "hidden": true,
            "title": "Response curves",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 9
        },
        "eval_tables/Evaluation-statistics_*.csv": {
            "title": "Model Evaluation",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 10
        },
        "eval_tables/Combined.Full.modelEvaluation.csv": {
            "title": "Model Evaluation",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 11
        },
        "eval/Full-presence-absence-plot_*.png": {
            "title": "Presence/absence density plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 12
        },
        "eval/Full-presence-absence-hist_*.png": {
            "title": "Presence background",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 13
        },
        "eval/Full-occurence_absence_pdf.png": {
            "title": "New Model plots",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 14
        },
        "eval/Full-TPR-TNR_*.png": {
            "title": "Sensitivity/Specificity plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 15
        },
        "eval/Full-error-rates_*.png": {
            "title": "Error rates plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 16
        },
        "eval/Full-ROC_*.png": {
            "title": "ROC plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 17
        },
        "eval/Full-loss-functions_*.png": {
            "title": "Loss functions plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 18
        },
        "eval_tables/Loss-function-intervals-table_*.csv": {
            "title": "Loss functions table",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 19
        },
        "eval/Full-loss-intervals.png": {
            "title": "Loss functions intervals",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 20
        },
        "eval_tables/Evaluation-data_*.csv": {
            "title": "Model evaluation data",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 21
        },
        "*.R": {
            "title": "Job Script",
            "genre": "JobScript",
            "mimetype": "text/x-r",
            "order": 30
        },
        "*.Rout": {
            "title": "Log file",
            "genre": "DataGenreLog",
            "mimetype": "text/x-r-transcript",
            "order": 32
        },
        "modelling_region.json": {
            "title": "modelling region",
            "hidden": true,
            "genre": "DataGenreSDMModellingRegion",
            "mimetype": "text/x-r-transcript",
            "order": 33
        },
        "metadata.json": {
            "title": "Metadata",
            "genre": "DataGenreMetadata",
            "mimetype": "application/json",
            "order": 34
        },
        "eval/Mean_sd_evaluation_*.png": {
            "title": "Mean and SD plot of model evaluation",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 35
        },
        "eval/Boxplot_evaluation_*.png": {
            "title": "Box-plots of model evaluation scores",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 36
        },
        "eval_tables/*.csv": {
            "title": "Model Evaluation",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 40
        },
        "eval/*.png": {
            "title": "New Model plots",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 50
        },
        "params.json": {
            "title": "Input parameters",
            "genre": "InputParams",
            "mimetype": "text/x-r-transcript",
            "order": 100
        },
        "model.object.rds.zip": {
            "files": ["*_model_object.rds",
                      "*/*.bccvl.models.out",
                      "*/.BIOMOD_DATA/bccvl/*",
                      "*/models/bccvl/*",
                      "*/proj_current/*.current.projection.out"
                      ],
            "title": "R SDM Model object",
            "genre": "DataGenreSDMModel",
            "mimetype": "application/zip",
            "order": 31
        }
    }
}
