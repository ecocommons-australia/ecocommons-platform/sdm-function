{
    "name": "Generalized Linear Model (GLM)",
    "description": "A regression model for data with a non-normal distribution, fitted with maximum likelihood estimation.",
    "algorithm_category": "statistical",
    "package_dependencies": "BIOMOD2",
    "input": {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "$id": "https://schema.ecocommons.org.au/sdm/glm.json",
        "title": "Generalized Linear Model (GLM) SDM Function Parameters",
        "description": "Schema for Generalized Linear Model (GLM) SDM Function parameters",
        "type": "object",
        "properties": {
            "species": {
                "title": "species occurrence dataset",
                "description": "UUID of species occurrence dataset",
                "type": "string"
            },
            "absence": {
                "title": "species absence dataset",
                "description": "UUID of species absence dataset",
                "type": [
                    "string",
                    "null"
                ],
                "default": null
            },
            "base_layer": {
                "title": "base layer dataset",
                "description": "UUID of bias dataset",
                "type": "object",
                "properties": {
                    "uuid": {
                        "title": "dataset id",
                        "description": "UUID of environmental dataset",
                        "type": "string",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    },
                    "layers": {
                        "title": "layers",
                        "description": "List of layer names",
                        "type": "array",
                        "items": {
                            "description": "Layer name",
                            "type": "string"
                        },
                        "minItems": 1,
                        "uniqueItems": true
                    }
                },
                "required": [
                    "uuid",
                    "layers"
                ],
                "default": null
            },
            "predictors": {
                "title": "predictors",
                "description": "List of environmental dataset UUID with a list of selected layer names.",
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "uuid": {
                            "title": "dataset id",
                            "description": "UUID of environmental dataset",
                            "type": "string",
                            "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                        },
                        "layers": {
                            "title": "layers",
                            "description": "List of layer names",
                            "type": "array",
                            "items": {
                                "description": "Layer name",
                                "type": "string"
                            },
                            "minItems": 1,
                            "uniqueItems": true
                        }
                    },
                    "required": [
                        "uuid",
                        "layers"
                    ]
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "modelling_region": {
                "title": "modelling region",
                "description": "Geojson object describing the geographic constraint region",
                "type": "object",
                "properties": {
                    "geojson": {
                        "type": "string",
                        "description": "Geojson string describing the geographic constraint region"
                    },
                    "method": {
                        "type": "string",
                        "description": "Method selected for generation",
                        "enum": [
                            "convex hull",
                            "pre-defined region",
                            "environmental envelope",
                            "user-drawn",
                            "shapefile"
                        ],
                        "default": "convex hull"
                    },
                    "radius": {
                        "type": "number",
                        "description": "Buffer radius in Km",
                        "minimum": 0.0,
                        "default": 0.0
                    },
                    "predefined_region": {
                        "type": "object",
                        "properties": {
                            "region_type": {
                                "type": "string",
                                "description": "Region type selected for pre-defined region"
                            },
                            "regions": {
                                "type": "array",
                                "items": {
                                    "type": "string"
                                },
                                "description": "Regions selected for pre-defined region",
                                "minItems": 1,
                                "uniqueItems": true
                            }
                        },
                        "required": [
                            "region_type",
                            "regions"
                        ]
                    }
                },
                "required": [
                    "geojson",
                    "method"
                ]
            },
            "scale_down": {
                "title": "scale down",
                "description": "Resample by scaling down the resolution : user-defined; finest-resolution (lowest resolution); coarsest-resolution (highest resolution)",
                "type": "string",
                "default": "finest-resolution"
            },
            "compress": {
                "title": "compression format",
                "description": "Compression format of objects stored.",
                "type": [
                    "string",
                    "null"
                ],
                "default": "gzip",
                "enum": [
                    "xz",
                    "gzip",
                    null
                ]
            },
            "pa_ratio": {
                "title": "absence-presence ratio",
                "description": "ratio of absence to presence points",
                "type": "number",
                "minimum": 0.0,
                "default": 1.0
            },
            "pa_strategy": {
                "title": "pseudo-absence strategy",
                "description": "strategy to generate pseudo-absence points: random; SRE (in sites with contrasting conditions to presences); disk (within a minimum and maximum distance from presences)",
                "type": "string",
                "default": "random",
                "enum": [
                    "random",
                    "sre",
                    "disk"
                ]
            },
            "pa_sre_quant": {
                "title": "pseudo-absence SRE quantile",
                "description": "quantile used for 'SRE' pseudo-absence generation strategy; default is 0.025",
                "type": "number",
                "minimum": 0.0,
                "default": 0.025
            },
            "pa_disk_min": {
                "title": "pseudo-absence disk minimum distance (m)",
                "description": "minimum distance (in metres) to presences for 'disk' pseudo-absence generation strategy",
                "type": "number",
                "minimum": 0.0,
                "default": 0.0
            },
            "pa_disk_max": {
                "title": "pseudo-absence disk maximum distance (m)",
                "description": "maximum distance (in metres) to presences for 'disk' pseudo-absence generation strategy",
                "type": [
                    "number",
                    "null"
                ],
                "minimum": 0.0,
                "default": null
            },
            "random_seed": {
                "title": "random seed",
                "description": "random seed used for generating random numbers. (algorithm parameter)",
                "type": [
                    "integer",
                    "null"
                ],
                "default": null,
                "minimum": -2147483648,
                "maximum": 2147483647
            },
            "nb_run_eval": {
                "title": "n-fold cross validation",
                "description": "n-fold cross validation. (algorithm parameter)",
                "type": "integer",
                "minimum": 1,
                "default": 10
            },
            "data_split": {
                "title": "data split",
                "description": "data split. (algorithm parameter)",
                "type": "integer",
                "minimum": 0,
                "maximum": 100,
                "default": 100
            },
            "prevalence": {
                "title": "weighted response weights",
                "description": "allows to give more or less weight to particular observations; default = NULL: each observation (presence or absence) has the same weight; if value < 0.5: absences are given more weight; if value > 0.5: presences are given more weight. (algorithm parameter)",
                "type": [
                    "number",
                    "null"
                ],
                "minimum": 0.0,
                "maximum": 1.0,
                "default": null
            },
            "var_import": {
                "title": "resampling",
                "description": "number of permutations to estimate the relative importance of each variable. (algorithm parameter)",
                "type": "integer",
                "minimum": 0,
                "default": 0
            },
            "rescale_all_models": {
                "title": "rescale all models",
                "description": "if true, all model prediction will be scaled with a binomial GLM. (algorithm parameter)",
                "type": "boolean",
                "default": false
            },
            "do_full_models": {
                "title": "do full models",
                "description": "if true, calibrate and evaluate models with the whole dataset. (algorithm parameter)",
                "type": "boolean",
                "default": true
            },
            "type": {
                "title": "regression type",
                "description": "type of regression to model: linear (\"simple\"), quadratic or polynomial. (algorithm parameter)",
                "type": "string",
                "default": "quadratic",
                "enum": [
                    "simple",
                    "quadratic",
                    "polynomial"
                ]
            },
            "interaction_level": {
                "title": "interaction level",
                "description": "number of interactions between predictor variables that need to be considered. (algorithm parameter)",
                "type": "integer",
                "default": 0
            },
            "test": {
                "title": "test criteria",
                "description": "criteria that should be used to test the fit of the model in stepwise predictor selection; if 'none' the stepwise procedure will be switched off. (algorithm parameter)",
                "type": "string",
                "default": "AIC",
                "enum": [
                    "AIC",
                    "BIC",
                    "none"
                ]
            },
            "family": {
                "title": "error distribution family",
                "description": "description of the error distribution of the response variable and the link function used in the model; needs to be 'binomial' for presence/absence data. (algorithm parameter)",
                "type": "string",
                "default": "binomial",
                "enum": [
                    "binomial",
                    "gaussian",
                    "gamma",
                    "inverse.gaussian",
                    "poisson",
                    "quasi",
                    "quasibinomial",
                    "quasipoisson"
                ]
            },
            "mustart": {
                "title": "mu-start",
                "description": "starting values for the vector of means. (algorithm parameter)",
                "type": "number",
                "default": 0.5
            },
            "control_epsilon": {
                "title": "epsilon",
                "description": "positive convergence tolerance e. (algorithm parameter)",
                "type": "number",
                "default": 1e-08
            },
            "control_maxit": {
                "title": "maximum MLE iterations",
                "description": "maximum number of IWLS iterations to find maximum likelihood estimates. (algorithm parameter)",
                "type": "integer",
                "default": 25
            },
            "control_trace": {
                "title": "MLE iteration output",
                "description": "whether output should be produced for each IWLS iteration. (algorithm parameter)",
                "type": "boolean",
                "default": false
            },
            "species_filter": {
                "title": "species filter",
                "description": "Species names to be included, only for Multi-Species SDM",
                "type": "array",
                "items": {
                    "description": "scientific name of species",
                    "type": "string"
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "subsets": {
                "title": "subsets",
                "description": "list of subset items, for Migratory Modelling",
                "type": "array",
                "items": {"$ref": "#/$defs/subset"},
                "minItems": 1,
                "uniqueItems": true
            },
            "unconstraint_map": {
                "title": "unconstraint map",
                "description": "Indicates whether to generate an unconstraint map or not. True by default.",
                "type": "boolean",
                "default": true
            },
            "generate_convexhull": {
                "title": "generate convex-hull polygon",
                "description": "Indicates to generate and apply a convex-hull polygon of the occurrence dataset to constraint. False by default.",
                "type": "boolean",
                "default": false
            }
        },
        "required": [
            "species",
            "absence",
            "predictors",
            "modelling_region",
            "scale_down",
            "compress",
            "pa_ratio",
            "pa_strategy",
            "pa_sre_quant",
            "pa_disk_min",
            "pa_disk_max",
            "random_seed",
            "nb_run_eval",
            "data_split",
            "prevalence",
            "var_import",
            "rescale_all_models",
            "do_full_models",
            "type",
            "interaction_level",
            "test",
            "family",
            "mustart",
            "control_epsilon",
            "control_maxit",
            "control_trace"
        ],
        "$defs": {
            "subset": {
                "title": "subset",
                "description": "subset for Migratory Modelling",
                "type": "array",
                "items": [
                    {"$ref": "#/$defs/subset_name"},
                    {"$ref": "#/$defs/month_filter"},
                    {"$ref": "#/$defs/subset_predictors"}
                ]
            },
            "subset_name": {
                "title": "subset name",
                "description": "name for a subset, only for Migratory Modelling",
                "type": "string"
            },
            "month_filter": {
                "title": "month filter",
                "description": "List of numerical months to use, only for Migratory Modelling",
                "type": "array",
                "items": {
                    "description": "Numerical month (1-12)",
                    "type": "integer",
                    "minimum": 1,
                    "maximum": 12
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "subset_predictors": {
                "title": "subset predictors",
                "type": "array",
                "description": "List of predictor items",
                "items": {
                    "$ref": "#/$defs/subset_predictor"
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "subset_predictor": {
                "title": "subset predictor",
                "description": "a reference to a selected curated dataset with some layers",
                "type": "object",
                "properties": {
                    "uuid": {
                        "title": "dataset id",
                        "type":  "string",
                        "description": "uuid of dataset in the database",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    },
                    "layers": {
                        "title": "layers",
                        "type": "array",
                        "items": {
                            "type": "string",
                            "description": "name of layer in the dataset"
                        },
                        "minItems": 1,
                        "uniqueItems": true
                    }
                },
                "required": ["uuid", "layers"]
            }
        }
    },
    "output": {
        "*/proj_current/proj_current_*.tif": {
            "title": "Projection to current climate",
            "mimetype": "image/geotiff",
            "layer": "projection_probability",
            "data_type": "Continuous"
        },
        "*/proj_current/proj_current_*_unconstrained.tif": {
            "title": "Projection to current climate - unconstrained",
            "mimetype": "image/geotiff",
            "layer": "projection_probability",
            "data_type": "Continuous"
        },
        "proj_current_*.png": {
            "title": "Projection plot",
            "mimetype": "image/png"
        },
        "proj_current_*_unconstrained.png": {
            "title": "Projection plot - unconstrained",
            "mimetype": "image/png"
        },
        "*/proj_current/proj_current_ClampingMask.tif": {
            "title": "Clamping Mask",
            "mimetype": "image/geotiff",
            "layer": "clamping_mask",
            "data_type": "Discrete"
        },
        "pseudo_absences_*.csv": {
            "title": "Absence records (map)",
            "mimetype": "text/csv"
        },
        "absence_*.csv": {
            "title": "Absence records (map)",
            "mimetype": "text/csv"
        },
        "occurrence_environmental_*.csv": {
            "title": "Occurrence points with environmental data",
            "mimetype": "text/csv"
        },
        "absence_environmental_*.csv": {
            "title": "Absence points with environmental data",
            "mimetype": "text/csv"
        },
        "mean_response_curves*.png": {
            "title": "Response curves",
            "mimetype": "image/png"
        },
        "*_mean_response_curves*.png": {
            "title": "Response curves",
            "mimetype": "image/png"
        },
        "variable_relative_contribution_*.png": {
            "title": "Variable relative contribution",
            "mimetype": "image/png"
        },
        "variable_correlations_*.png": {
            "title": "Variable correlations",
            "mimetype": "image/png"
        },
        "Evaluation-statistics_*.csv": {
            "title": "Model Evaluation",
            "mimetype": "text/csv"
        },
        "combined.Full.modelEvaluation.csv": {
            "title": "Model Evaluation",
            "mimetype": "text/csv"
        },
        "Full-presence-absence-plot_*.png": {
            "title": "Presence/absence density plot",
            "mimetype": "image/png"
        },
        "Full-presence-absence-hist_*.png": {
            "title": "Presence/absence density histogram",
            "mimetype": "image/png"
        },
        "Full-occurence_absence_pdf.png": {
            "title": "New Model plots",
            "mimetype": "image/png"
        },
        "Full-TPR-TNR_*.png": {
            "title": "Sensitivity/Specificity plot",
            "mimetype": "image/png"
        },
        "Full-error-rates_*.png": {
            "title": "Error rates plot",
            "mimetype": "image/png"
        },
        "Full-ROC_*.png": {
            "title": "ROC plot",
            "mimetype": "image/png"
        },
        "Full-loss-functions_*.png": {
            "title": "Loss functions plot",
            "mimetype": "image/png"
        },
        "Loss-function-intervals-table_*.csv": {
            "title": "Loss functions table",
            "mimetype": "text/csv"
        },
        "Full-loss-intervals_*.png": {
            "title": "Loss functions intervals",
            "mimetype": "image/png"
        },
        "Evaluation-data_*.csv": {
            "title": "Model evaluation data",
            "mimetype": "text/csv"
        },
        "*.R": {
            "title": "Job Script",
            "mimetype": "text/x-r"
        },
        "*.Rout": {
            "title": "Log file",
            "mimetype": "text/x-r-transcript"
        },
        "modelling_region.json": {
            "title": "modelling region",
            "mimetype": "text/x-r-transcript"
        },
        "metadata.json": {
            "title": "Metadata",
            "mimetype": "application/json"
        },
        "*.csv": {
            "title": "Model Evaluation",
            "mimetype": "text/csv"
        },
        "*.png": {
            "title": "New Model plots",
            "mimetype": "image/png"
        },
        "params.json": {
            "title": "Input parameters",
            "mimetype": "text/x-r-transcript"
        },
        "Mean_sd_evaluation_*.png": {
          "title": "Mean and SD plot of model evaluation",
          "mimetype": "image/png"
        },
        "Boxplot_evaluation_*.png": {
          "title": "Box-plots of model evaluation scores",
          "mimetype": "image/png"
        },
        "Summary_GLM.txt": {
          "title": "GLM model summary",
          "mimetype": "text/plain"
        },
        "model.object.RData.zip": {
            "files": [
                "model.object.RData",
                "*/*.bccvl.models.out",
                "*/.BIOMOD_DATA/bccvl/*",
                "*/models/bccvl/*",
                "*/proj_current/*.current.projection.out"
            ],
            "title": "R SDM Model object",
            "mimetype": "application/zip"
        }
    }
}