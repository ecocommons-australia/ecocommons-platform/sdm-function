{
    "name": "Species Trait GLMM Function",
    "description": "The Species Trait GLMM Function can be used to reduce the uncertainty of using a single-algorithm, or single-climate-model approach to investigate climate change impacts on biodiversity.",
    "algorithm_category": "species trait",
    "package_dependencies": "lme4, ordinal",
    "input": {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "$id": "https://schema.ecocommons.org.au/sdm/speciestrait_glmm.json",
        "title": "Species Trait GLMM Function Parameters",
        "description": "Schema for Species Trait GLMM Function parameters",
        "type": "object",
        "properties": {
            "traits_dataset": {
                "title": "traits dataset",
                "type": "string",
                "description": "UUID of the dataset with the species trait data",
                "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
            },
            "traits_dataset_params": {
                "title": "traits dataset parameters",
                "type": "object",
                "description": "selected trait parameters from the traits dataset",
                "additionalProperties": {
                    "type": "string",
                    "enum": [
                        "species",
                        "trait_con",
                        "trait_ord",
                        "trait_nom",
                        "env_var_con",
                        "env_var_cat",
                        "random_con",
                        "random_cat"
                    ]
                }
            },
            "family":  {
                "title": "error distribution family",
                "description": "error distribution and link function to be used in the model",
                "type": "string",
                "enum": [
                    "binomial",
                    "gaussian",
                    "gamma",
                    "poisson",
                    "binomial(link=\"logit\")",
                    "binomial(link=\"probit\")",
                    "gaussian(link=\"identity\")",
                    "gaussian(link=\"log\")",
                    "gaussian(link=\"inverse\")",
                    "Gamma(link=\"identity\")",
                    "Gamma(link=\"log\")",
                    "Gamma(link=\"inverse\")",
                    "poisson(link=\"identity\")",
                    "poisson(link=\"log\")",
                    "poisson(link=\"sqrt\")"
                ],
                "default": "gaussian(link=\"identity\")"
            },
            "na_action":  {
                "title": "NA action",
                "description": "controls how to handle missing data; default = na.omit: records with missing values are removed; na.fail: model will only run if data contains no missing values; na.exclude: missing values are not used in model but maintained for residuals and fitted values",
                "type": "string",
                "enum": [
                    "na.fail",
                    "na.omit",
                    "na.exclude"
                ],
                "default": "na.omit"
            },
            "method":  {
                "title": "model fitting method",
                "description": "method to be used in fitting the model; default = glm.fit, which uses the IWLS method; model.frame does no fitting",
                "type": "string",
                "enum": [
                    "glm.fit",
                    "model.frame"
                ],
                "default": "glm.fit"
            },
            "random_seed": {
                "title": "random seed",
                "description": "seed used for generating random numbers.",
                "type": [
                    "integer",
                    "null"
                ],
                "default": null,
                "minimum": -2147483648,
                "maximum": 2147483647
            },
            "species": {
                "title": "selected species",
                "description": "List of species names to use to run the species-trait function",
                "type": [
                    "array",
                    "null"
                ],
                "items": {
                    "description": "species name",
                    "type": "string"
                }, 
                "default": null
            },
            "predictors": {
                "title": "predictors",
                "description": "List of environmental dataset UUID with a list of selected layer names.",
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "uuid": {
                            "title": "dataset id",
                            "description": "UUID of environmental dataset",
                            "type": "string",
                            "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                        },
                        "layers": {
                            "title": "layers",
                            "description": "List of layer names",
                            "type": "array",
                            "items": {
                                "description": "Layer name",
                                "type": "string"
                            },
                            "minItems": 1,
                            "uniqueItems": true
                        }
                    },
                    "required": [
                        "uuid",
                        "layers"
                    ]
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "modelling_region": {
                "title": "modelling region",
                "description": "Geojson object describing the geographic constraint region",
                "type": "object",
                "properties": {
                    "geojson": {
                        "type": "string",
                        "description": "Geojson string describing the geographic constraint region"
                    },
                    "method": {
                        "type": "string",
                        "description": "Method selected for generation",
                        "enum": [
                            "convex hull",
                            "pre-defined region",
                            "environmental envelope",
                            "user-drawn",
                            "shapefile"
                        ],
                        "default": "convex hull"
                    },
                    "radius": {
                        "type": "number",
                        "description": "Buffer radius in Km",
                        "minimum": 0.0,
                        "default": 0.0
                    },
                    "predefined_region": {
                        "type": "object",
                        "properties": {
                            "region_type": {
                                "type": "string",
                                "description": "Region type selected for pre-defined region"
                            },
                            "regions": {
                                "type": "array",
                                "items": {
                                    "type": "string"
                                },
                                "description": "Regions selected for pre-defined region",
                                "minItems": 1,
                                "uniqueItems": true
                            }
                        },
                        "required": [
                            "region_type",
                            "regions"
                        ]
                    }
                },
                "required": [
                    "geojson",
                    "method"
                ]
            }
        },
        "required": [
            "traits_dataset",
            "traits_dataset_params",
            "family",
            "method",
            "species",
            "predictors",
            "modelling_region"
        ]
    },
    "output": {
        "*_results.txt": {
            "title": "Model summary",
            "mimetype": "text/plain"
        },
        "*_trait_environmental.csv": {
            "title": "Trait data with environmental data",
            "mimetype": "text/csv"
        },
        "*.R": {
            "title": "Job Script",
            "mimetype": "text/x-r"
        },
        "*.Rout": {
            "title": "Log file",
            "mimetype": "text/x-r-transcript"
        },
        "modelling_region.json": {
            "title": "modelling region",
            "hidden": true,
            "mimetype": "text/x-r-transcript"
        },
        "metadata.json": {
            "title": "Metadata",
            "mimetype": "application/json"
        },
        "*.RData": {
            "title": "R Species Traits Model object",
            "mimetype": "application/x-r-data"
        },
        "params.json": {
            "title": "Input parameters",
            "mimetype": "text/x-r-transcript"
        }
    }
}
