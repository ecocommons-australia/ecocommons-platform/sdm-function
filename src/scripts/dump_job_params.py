import os
import json
import logging
from django.core.management.base import CommandError
from .core.utils import fetch_job

log = logging.getLogger(__name__)

SRC_DIR = os.path.dirname(os.path.dirname(__file__))
CURRENT_DIR = os.getcwd()


def usage():
    usage_str = "Usage: dump_job_params --script-args \n\
                jobuuid=<jobuuid> \n"
    print(usage_str)
    raise CommandError(1)


def run(*args):
    opts = dict([i.split('=') for i in args])
    job_uuid = opts.get('jobuuid')
    if not (job_uuid):
        usage()

    job = fetch_job(job_uuid)

    paramfile = os.path.join(CURRENT_DIR, 'params.json')
    with open(paramfile, 'w') as f:
        f.write(json.dumps(job.get('parameters'), indent=4))
        log.info(f"Written '{paramfile}'")
