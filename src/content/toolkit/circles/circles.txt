{
    "input": {
        "species": {
            "rename": "species_occurrence_dataset",
            "type": "dataset",
            "link_param": "species_filter"
        },
        "absence": {
            "rename": "species_absence_dataset",
            "type": "dataset",
            "link_param": "species_filter"
        },
        "bias": {
            "rename": "bias_dataset",
            "type": "dataset"
        },
        "base_layer": {
            "type": "dataset_layer",
            "plural": "single"
        },
        "predictors": {
            "rename": "environmental_datasets",
            "type": "dataset_layer"
        },
        "modeling_id": {
            "type": "parameter",
            "default": "bccvl"
        },
        "selected_models": {
            "type": "parameter",
            "default": "all"
        },
        "generate_convexhull": {
            "type": "parameter",
            "default": false
        },
        "unconstraint_map": {
            "type": "parameter",
            "default": true
        },
        "modelling_region": {
            "type": "parameter",
            "download_as": {
                "key": "geojson",
                "rekey": "filename",
                "filename": "modelling_region.json",
                "transform": ["fetch_ala_geojson"]
            }
        }
    },
    "script": [
        "{function}/{function}.R"
    ],
    "metadata": {
        "Species occurrence": "species",
        "True absence": "absence",
        "Bias data": "bias",
        "Predictors": "predictors",
        "Citation": "default"
    },
    "output": {
        "files": {
            "Rplots.pdf": {
                "skip": true
            },
            "eval/AUC.png": {
                "skip": true
            },
            "results.html": {
                "skip": true
            },
            "eval/Proj_current*.tif": {
                "title": "Projection",
                "genre": "DataGenreCP",
                "mimetype": "image/geotiff",
                "layer": "projection_binary",
                "data_type": "Discrete",
                "order": 1
            },
            "eval/Proj_current*_unconstrained.tif": {
                "title": "Projection - unconstrained",
                "genre": "DataGenreCP_ENVLOP",
                "mimetype": "image/geotiff",
                "layer": "projection_binary",
                "data_type": "Discrete",
                "order": 2
            },
            "eval/Proj_current_*.png": {
                "title": "Projection plot",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 3
            },
            "eval/Proj_current_*_unconstrained.png": {
                "title": "Projection plot - unconstrained",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 4
            },
            "eval_tables/Pseudo_absences_*.csv": {
                "title": "Absence records (map)",
                "genre": "DataGenreSpeciesAbsence",
                "mimetype": "text/csv",
                "order": 6
            },
            "eval_tables/Absence_*.csv": {
                "title": "Absence records (map)",
                "genre": "DataGenreSpeciesAbsence",
                "mimetype": "text/csv",
                "order": 6
            },
            "eval_tables/Occurrence_environmental_*.csv": {
                "title": "Occurrence points with environmental data",
                "genre": "DataGenreSpeciesOccurEnv",
                "mimetype": "text/csv",
                "order": 5
            },
            "eval_tables/Absence_environmental_*.csv": {
                "title": "Absence points with environmental data",
                "genre": "DataGenreSpeciesAbsenceEnv",
                "mimetype": "text/csv",
                "order": 7
            },
            "eval/Response_curve_*.png": {
                "title": "Marginal Response Curve",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 8
            },
            "eval/*_response_curve_*.png": {
                "hidden": true,
                "title": "Marginal Response Curve",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 8
            },
            "eval_tables/Biomod2_like_VariableImportance.csv": {
                "title": "Model accuracy statistics",
                "genre": "DataGenreSDMEval",
                "mimetype": "text/csv",
                "order": 9
            },
            "eval_tables/Maxent_like_VariableImportance.csv": {
                "title": "Model accuracy statistics",
                "genre": "DataGenreSDMEval",
                "mimetype": "text/csv",
                "order": 10
            },
            "eval_tables/Combined.modelEvaluation.csv": {
                "title": "Model accuracy statistics",
                "genre": "DataGenreSDMEval",
                "mimetype": "text/csv",
                "order": 11
            },
            "eval/Bioclim-ppp.png": {
                "title": "New Model plots",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 12
            },
            "eval/Full-occurrence_absence_hist.png": {
                "title": "New Model plots",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 13
            },
            "eval/Full-true_and_false_posivite_rates.png": {
                "title": "New Model plots",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 14
            },
            "eval/Bioclim-error.png": {
                "title": "New Model plots",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 15
            },
            "eval/Bioclim-roc.png": {
                "title": "New Model plots",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 16
            },
            "eval/Bioclim-tradeoffs.png": {
                "title": "New Model plots",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 17
            },
            "eval_tables/Evaluation.summary.csv": {
                "title": "Model Evaluation",
                "genre": "DataGenreSDMEval",
                "mimetype": "text/csv",
                "order": 18
            },
            "eval/Bioclim-intervals.png": {
                "title": "New Model plots",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 19
            },
            "eval_tables/Evaluation.performance.csv": {
                "title": "Model Evaluation",
                "genre": "DataGenreSDMEval",
                "mimetype": "text/csv",
                "order": 20
            },
            "*.R": {
                "title": "Job Script",
                "genre": "JobScript",
                "mimetype": "text/x-r",
                "order": 21
            },
            "*_model_object.rds": {
                "title": "R SDM Model object",
                "genre": "DataGenreSDMModel",
                "mimetype": "application/x-r-data",
                "order": 23
            },
            "*.Rout": {
                "title": "Log file",
                "genre": "DataGenreLog",
                "mimetype": "text/x-r-transcript",
                "order": 24
            },
            "modelling_region.json": {
                "title": "modelling region",
                "hidden": true,
                "genre": "DataGenreSDMModellingRegion",
                "mimetype": "text/x-r-transcript",
                "order": 30
            },
            "metadata.json": {
                "title": "Metadata",
                "genre": "DataGenreMetadata",
                "mimetype": "application/json",
                "order": 31
            },
            "eval_tables/*.csv": {
                "title": "Model accuracy statistics",
                "genre": "DataGenreSDMEval",
                "mimetype": "text/csv",
                "order": 40
            },
            "eval/*.png": {
                "title": "New Model plots",
                "genre": "DataGenreSDMEval",
                "mimetype": "image/png",
                "order": 50
            },
            "params.json": {
                "title": "Input parameters",
                "genre": "InputParams",
                "mimetype": "text/x-r-transcript",
                "order": 100
            }
        }
    }
}
