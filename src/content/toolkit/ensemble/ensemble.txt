{
    "input": {
        "resultsets": {
            "type": "jobs",
            "rename": {
                "datasets": {
                    "type": "resultsets",
                    "titles": [
                        "Projection to current climate",
                        "Predicted habitat suitability under current conditions based on cloglog output",
                        "Future Projection map"
                    ],
                    "search_op": "ANY"
                },
                "thresholds": {
                    "type": "parameter"
                },
                "sdm_projections": {
                    "type": "parameter"
                }
            }
        }
    },
    "script": [
        "{function}/{function}.R"
    ],
    "metadata": {
        "Job": "default",
        "SDM/CC projection inputs": "resultsets",
        "Parameters": "default",
        "Citation": "default"
    },
    "output": {
        "files": {
            "Ensemble_mean_*.tif": {
                "title": "Summary Mean",
                "genre": "DataGenreEnsembleResult",
                "layer": "mean_ensemble_projection_probability",
                "data_type": "Continuous",
                "mimetype": "image/geotiff",
                "order": 1
            },
            "Ensemble_min_*.tif": {
                "title": "Summary Minimum",
                "genre": "DataGenreEnsembleResult",
                "layer": "projection_probability",
                "data_type": "Continuous",
                "mimetype": "image/geotiff",
                "order": 2
            },
            "Ensemble_max_*.tif": {
                "title": "Summary Maximum",
                "genre": "DataGenreEnsembleResult",
                "layer": "projection_probability",
                "data_type": "Continuous",
                "mimetype": "image/geotiff",
                "order": 3
            },
            "Ensemble_variance_*.tif": {
                "title": "Summary Variance",
                "genre": "DataGenreEnsembleResult",
                "layer": "projection_probability",
                "data_type": "Continuous",
                "mimetype": "image/geotiff",
                "order": 4
            },
            "Ensemble_q0p05_*.tif": {
                "title": "5th Percentile",
                "genre": "DataGenreEnsembleResult",
                "layer": "projection_probability",
                "data_type": "Continuous",
                "mimetype": "image/geotiff",
                "order": 5
            },
            "Ensemble_q0p1_*.tif": {
                "title": "10th Percentile",
                "genre": "DataGenreEnsembleResult",
                "layer": "projection_probability",
                "data_type": "Continuous",
                "mimetype": "image/geotiff",
                "order": 6
            },
            "Ensemble_q0p5_*.tif": {
                "title": "50th Percentile",
                "genre": "DataGenreEnsembleResult",
                "layer": "projection_probability",
                "data_type": "Continuous",
                "mimetype": "image/geotiff",
                "order": 7
            },
            "Ensemble_q0p9_*.tif": {
                "title": "90th Percentile",
                "genre": "DataGenreEnsembleResult",
                "layer": "projection_probability",
                "data_type": "Continuous",
                "mimetype": "image/geotiff",
                "order": 8
            },
            "Ensemble_q0p95_*.tif": {
                "title": "95th Percentile",
                "genre": "DataGenreEnsembleResult",
                "layer": "projection_probability",
                "data_type": "Continuous",
                "mimetype": "image/geotiff",
                "order": 9
            },
            "Ensemble_probchange_*.tif": {
                "title": "Change in probability map",
                "genre": "DataGenreClimateChangeMetricMap",
                "layer": "probability_difference",
                "data_type": "Continuous",
                "mimetype": "image/geotiff",
                "order": 10
            },
            "Ensemble_rangechange_*.tif": {
                "title": "Change in species range map",
                "genre": "DataGenreClimateChangeMetricMap",
                "layer": "range_change",
                "data_type": "Discrete",
                "mimetype": "image/geotiff",
                "order": 11
            },
            "Ensemble_rangechange_*.csv": {
                "title": "Change in species range table",
                "genre": "DataGenreClimateChangeMetric",
                "mimetype": "text/csv",
                "order": 12
            },
            "Ensemble_meansdm_*.tif": {
                "title": "SDM mean",
                "genre": "DataGenreEnsembleResult",
                "layer": "mean_sdm_projection_probability",
                "data_type": "Continuous",
                "mimetype": "image/geotiff",
                "order": 13
            },
            "*.R": {
                "title": "Job Script",
                "genre": "JobScript",
                "mimetype": "text/x-r",
                "order": 20
            },
            "*.Rout": {
                "title": "Log file",
                "genre": "DataGenreLog",
                "mimetype": "text/x-r-transcript",
                "order": 21
            },
            "metadata.json": {
                "title": "Metadata",
                "genre": "DataGenreMetadata",
                "mimetype": "application/json",
                "order": 22
            }
        }
    }
}