{
    "input": {
        "species_distribution_models": {
            "type": "job",
            "order": 1,
            "rename": {
                "species_distribution_models": {
                    "type": "modelfile",
                    "key": "uuid",
                    "title": ["R SDM Model object"]
                },
                "sdm_projections": {
                    "type": "projectionfile",
                    "title": [
                        "Projection to current climate",
                        "Predicted habitat suitability under current conditions based on cloglog output"
                    ],
                    "search_op": "ANY"
                },
                "threshold": {
                    "type": "parameter",
                    "key": "threshold"
                },
                "sdm_function": {
                    "type": "parameter",
                    "key": "algorithm"
                },
                "species": {
                    "type": "rsparameter",
                    "key": "scientificName"
                },
                "subset": {
                    "type": "mdparameter",
                    "key": "subset"
                },
                "tails": {
                    "type": "mdparameter",
                    "key": "tails"
                }
            }
        },
        "future_climate_datasets": {
            "type": "future_dataset_layer",
            "order": 2,
            "rename": {
                "future_climate_datasets": {
                    "type": "dataset_layer"
                },
                "selected_future_layers": {
                    "type": "selected_layers"
                },
                "gcm": {
                    "type": "mdparameter",
                    "key": "gcm"
                },
                "emsc": {
                    "type": "mdparameter",
                    "key": "emsc"
                },
                "month": {
                    "type": "mdparameter",
                    "key": "month"
                },
                "year": {
                    "type": "mdparameter",
                    "key": "year"
                },
                "projection_name": {
                    "type": "create_parameter",
                    "name": "{emsc}_{gcm}_{year}",
                    "nameparts": ["emsc", "gcm", "year"]
                }
            }
        },
        "selected_models": {
            "type": "parameter",
            "default": "all"
        },
        "projection_region": {
            "type": "parameter",
            "download_as": {
                "key": "geojson",
                "rekey": "filename",
                "filename": "projection_region.json",
                "transform": ["fetch_ala_geojson"]
            }
        }
    },
    "script": [
        "{function}/{function}.R"
    ],
    "metadata": {
        "Job": "default",
        "Species Distribution Model": "species_distribution_models",
        "Future Predictors": {
            "param": "future_climate_datasets",
            "link_pparam": "selected_future_layers"
        },
        "Parameters": "default",
        "Citation": "default"
    },
    "output": {
        "files": {
            "Rplots.pdf": {
                "skip": true
            },
            "Proj_*.tif": {
                "title": "Future Projection map",
                "genre": "DataGenreFP",
                "layer": "",
                "data_type": "",
                "mimetype": "image/geotiff",
                "order": 1
            },
            "*/proj_*/Proj_*.tif": {
                "title": "Future Projection map",
                "genre": "DataGenreFP",
                "layer": "",
                "data_type": "",
                "mimetype": "image/geotiff",
                "order": 1
            },
            "Proj_*_unconstrained.tif": {
                "title": "Unconstraint Future Projection map",
                "genre": "DataGenreFP_ENVLOP",
                "layer": "",
                "data_type": "",
                "mimetype": "image/geotiff",
                "order": 2
            },
            "*/proj_*/Proj_*_unconstrained.tif": {
                "title": "Unconstraint Future Projection map",
                "genre": "DataGenreFP_ENVLOP",
                "layer": "",
                "data_type": "",
                "mimetype": "image/geotiff",
                "order": 2
            },
            "Proj_*.png": {
                "title": "Future Projection plot",
                "genre": "DataGenreFP_PLOT",
                "mimetype": "image/png",
                "order": 3
            },
            "*/proj_*/Proj_*.png": {
                "title": "Future Projection plot",
                "genre": "DataGenreFP_PLOT",
                "mimetype": "image/png",
                "order": 3
            },
            "*/proj_*/Proj_*_ClampingMask.tif": {
                "title": "Clamping Mask",
                "genre": "DataGenreClampingMask",
                "mimetype": "image/geotiff",
                "layer": "clamping_mask",
                "data_type": "Discrete",
                "order": 4
            },
            "Prob_change_*.tif": {
                "title": "Change in probability map",
                "genre": "DataGenreClimateChangeMetricMap",
                "layer": "probability_difference",
                "data_type": "Continuous",
                "mimetype": "image/geotiff",
                "order": 5
            },
            "*/proj_*/Prob_change_*.tif": {
                "title": "Change in probability map",
                "genre": "DataGenreClimateChangeMetricMap",
                "layer": "probability_difference",
                "data_type": "Continuous",
                "mimetype": "image/geotiff",
                "order": 5
            },
            "Range_change_*.tif": {
                "title": "Change in species range map",
                "genre": "DataGenreClimateChangeMetricMap",
                "layer": "range_change",
                "data_type": "Discrete",
                "mimetype": "image/geotiff",
                "order": 6
            },
            "*/proj_*/Range_change_*.tif": {
                "title": "Change in species range map",
                "genre": "DataGenreClimateChangeMetricMap",
                "layer": "range_change",
                "data_type": "Discrete",
                "mimetype": "image/geotiff",
                "order": 6
            },
            "Range_change_*.csv": {
                "title": "Change in species range table",
                "genre": "DataGenreClimateChangeMetric",
                "mimetype": "text/csv",
                "order": 7
            },
            "*/proj_*/Range_change_*.csv": {
                "title": "Change in species range table",
                "genre": "DataGenreClimateChangeMetric",
                "mimetype": "text/csv",
                "order": 7
            },
            "Centre_species_range_*.csv": {
                "title": "Change in centre of species range table",
                "genre": "DataGenreClimateChangeMetric",
                "mimetype": "text/csv",
                "order": 8
            },
            "*/proj_*/Centre_species_range_*.csv": {
                "title": "Change in centre of species range table",
                "genre": "DataGenreClimateChangeMetric",
                "mimetype": "text/csv",
                "order": 8
            },
            "*.R": {
                "title": "Job Script",
                "genre": "JobScript",
                "mimetype": "text/x-r",
                "order": 9
            },
            "*.Rout": {
                "title": "Log file",
                "genre": "DataGenreLog",
                "mimetype": "text/x-r-transcript",
                "order": 10
            },
            "metadata.json": {
                "title": "Metadata",
                "genre": "DataGenreMetadata",
                "mimetype": "application/json",
                "order": 11
            },
            "params.json": {
                "title": "Input parameters",
                "genre": "InputParams",
                "mimetype": "text/x-r-transcript",
                "order": 100
            }
        }
    }
}
